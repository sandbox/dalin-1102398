<?php

/**
 * Implements hook_block_info().
 */
function snownpo_block_info() {
  $blocks['recent_blogs'] = array(
    'info' => t('Recent blog post'),
    'cache' => DRUPAL_CACHE_GLOBAL,
    'status' => 1,
    'region' => 'sidebar_first',
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'pages' => '<front>',
  );
  $blocks['featured_project'] = array(
    'info' => t('Featured project'),
    'cache' => DRUPAL_CACHE_GLOBAL,
    'status' => 1,
    'region' => 'content',
    'weight' => 10,
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'pages' => '<front>',
  );
  $blocks['experts'] = array(
    'info' => t('Experts'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
    'status' => 1,
    'region' => 'sidebar_second',
    'visibility' => BLOCK_VISIBILITY_LISTED,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function snownpo_block_view($delta = '') {
  $block = array();
  $function_name = __FUNCTION__ . '_' . $delta;
  if (function_exists($function_name)) {
    return $function_name();
  }
}

/**
 * Block callback.
 */
function snownpo_block_view_recent_blogs() {
  $block['subject'] = t('Latest News');
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'blog_entry')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 2)
    ->execute();
  $nids = array();
  foreach($results['node'] as $node) {
    $nids[] = $node->nid;
  }
  $nodes = node_load_multiple($nids);
  $build = array();
  foreach ($nodes as $node) {
    $block['content'][] = node_view($node, 'teaser');
  }
  return $block;
}

/**
 * Block callback.
 */
function snownpo_block_view_featured_project() {
  $block['subject'] = t('Featured Project');
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'project')
    ->propertyCondition('promote', TRUE)
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 1)
    ->execute();
  $node = node_load(reset($results['node'])->nid);
  $block['content'] = array(
    'node' => node_view($node, 'teaser'),
    '#prefix' => '<div style="background: #F6F6F2; padding:0.1em 1.5em;">',
    '#suffix' => '</div>',
  );
  return $block;
}

/**
 * Block callback.
 */
function snownpo_block_view_experts() {
  // Get a list of countries that apply to the current page.
  $countries_vocab = taxonomy_vocabulary_machine_name_load('countries');

  $tids = array();
  if (($term = menu_get_object('taxonomy_term', 2)) && $term->vid == $countries_vocab->vid) {

    // Get a list of term IDs (people) that are experts in this country.
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'taxonomy_term')
      ->fieldCondition('field_expertise', 'tid', $term->tid)
      ->execute();

    // Flatten the array of term IDs.
    $tids = array();
    if (isset($results['taxonomy_term'])) {
      foreach($results['taxonomy_term'] as $term) {
        $tids[] = $term->tid;
      }
    }

    // Turn the tids into renderable content.
    $people = taxonomy_term_load_multiple($tids);
    $block['content'] = array();
    foreach ($people as $person) {
      $block['content'][] = taxonomy_term_view($person, 'teaser');
    }

  }
  
  $block['subject'] = t('Experts');
  return $block;
}

/**
 * Implements hook_menu().
 */
function snownpo_menu() {
  $items['blog'] = array(
    'page callback' => 'snownpo_blog_page',
    'title' => 'Blog',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'main-menu',
    'access arguments' => array('access content'),
    'weight' => 3,
  );
  $items['people'] = array(
    'page callback' => 'snownpo_people_page',
    'title' => 'People',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'main-menu',
    'access arguments' => array('access content'),
    'weight' => 4,
  );
  $items['projects'] = array(
    'page callback' => 'snownpo_projects_page',
    'title' => 'Projects',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'main-menu',
    'access arguments' => array('access content'),
    'weight' => 5,
  );
  $items['countries'] = array(
    'page callback' => 'snownpo_countries_page',
    'title' => 'Countries',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'main-menu',
    'access arguments' => array('access content'),
    'weight' => 6,
  );
  return $items;
}

/**
 * Page callback to show projects.
 */
function snownpo_projects_page() {
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'project')
    ->propertyOrderBy('created', 'DESC')
    ->pager(10, 1)
    ->execute();
  $nids = array();
  foreach($results['node'] as $node) {
    $nids[] = $node->nid;
  }
  $nodes = node_load_multiple($nids);
  $build = array();
  foreach ($nodes as $node) {
    $build[] = node_view($node, 'teaser');
  }
  $build[]['#markup'] = theme('pager', array('element' => 1));
  return $build;
}

/**
 * Page callback to show blog.
 */
function snownpo_blog_page() {
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'blog_entry')
    ->propertyOrderBy('created', 'DESC')
    ->pager(10, 1)
    ->execute();
  $nids = array();
  foreach($results['node'] as $node) {
    $nids[] = $node->nid;
  }
  $nodes = node_load_multiple($nids);
  $build = array();
  foreach ($nodes as $node) {
    $build[] = node_view($node, 'teaser');
  }
  $build[]['#markup'] = theme('pager', array('element' => 1));
  return $build;
}

/**
 * Page callback to show people.
 */
function snownpo_people_page() {
  $build = array();
  $people_vocab = taxonomy_vocabulary_machine_name_load('people');

  // Get a list of all roles.
  $roles_vocab = taxonomy_vocabulary_machine_name_load('roles');
  $roles = taxonomy_get_tree($roles_vocab->vid, 0, 1);

  // For each role generate a list of people in that role.
  foreach ($roles as $role) {

    // Get a list of term IDs (people) in this role.
    $query = new EntityFieldQuery();
    $results = $query->entityCondition('entity_type', 'taxonomy_term')
      ->fieldCondition('field_role', 'tid', $role->tid)
      ->execute();

    // Flatten the array of term IDs.
    $tids = array();
    if (isset($results['taxonomy_term'])) {
      foreach($results['taxonomy_term'] as $term) {
        $tids[] = $term->tid;
      }
    }

    // Turn the tids into renderable content.
    $people = taxonomy_term_load_multiple($tids);
    $list = array();
    foreach ($people as $person) {
      $list[] = taxonomy_term_view($person, 'teaser');
    }

    // Prepend a title if there were results.
    if (!empty($list)) {
      array_unshift($list, array(
        '#markup' => $role->name .'s',
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ));
    }
    $build[] = $list;
  }
  
  return $build;
}

/**
 * Page callback to show countries.
 */
function snownpo_countries_page() {
  $build = array();
  $counties_vocab = taxonomy_vocabulary_machine_name_load('countries');

  // Get a list of term IDs (countries).
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('vid', $counties_vocab->vid)
    ->execute();

  // Flatten the array of term IDs.
  $tids = array();
  if (isset($results['taxonomy_term'])) {
    foreach($results['taxonomy_term'] as $term) {
      $tids[] = $term->tid;
    }
  }

  // Turn the tids into renderable content.
  $countries = taxonomy_term_load_multiple($tids);
  foreach ($countries as $country) {
    $build[] = taxonomy_term_view($country, 'teaser');
  }

  return $build;
}
